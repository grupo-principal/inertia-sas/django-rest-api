# Django REST API
## Contenido

---

## Instalación python, pip y venv
### Instalación Gestor de Paquetes Homebrew para macOS
Instalamos el gestor de paquetes

````zsh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
````
### Instalación Python 3, Gestor de Paquetes de Python y Entorno Virtual en macOS
Instalamos python 3.8 utilizando el Homebrew. Python ya tiene incluido el gestor de paquetes de python `pip3` y el entorno virtual `venv`.

````
brew install python@3.8
````

Si existen otras versiones de python instaladas, las desvinculamos.

````
brew unlink python@3.x
````

Vinculamos python 3.8

````
brew link --force python@3.8
````

### Instalación Python 3, Gestor de Paquetes de Python y Entorno Virtual en ubuntu 20.04
Actualizamos y refrezcamos las listas de repositorio

````
sudo apt-get update
````

Instalamos `python`.

````
sudo apt-get install python3.8
````

Instalamos el gestor de paquetes de python `pip`

````
sudo apt-get install python3-pip
````

Instalamos el entorno virtual `venv`

````
sudo apt-get install python3.8-venv
````

### Fuentes
- [Instala Homebrew en macOS](https://brew.sh/index_es)
- [Instala python@3.8 en macOS con Homebrew](https://formulae.brew.sh/formula/python@3.8)
- [Instala python3.8 en Ubuntu 20.04](https://ubuntu.pkgs.org/20.04/ubuntu-main-amd64/python3.8_3.8.2-1ubuntu1_amd64.deb.html)
- [Instala pip y venv en Ubuntu 20.04](https://packaging.python.org/guides/installing-using-linux-tools/#debian-ubuntu)


## Crear y Activar el Entorno Virtual (.venv)
Creamos el entorno virtual

````
python3 -m venv .venv
````
Activamos el entorno virtual

````
source .venv/bin/activate

````

Desactivamos el entorno virtual

````
deactivate
````

### Fuentes
- [Entornos Virtuales y Paquetes](https://docs.python.org/es/3/tutorial/venv.html)
- [Creación de Entornos Virtuales](https://docs.python.org/es/3/library/venv.html#)

---

## Cookiecutter Django
### Caraterísticas

- For Django 3.1
- Works with Python 3.9
- Renders Django projects with 100% starting test coverage
- Twitter __Bootstrap__ v5 (maintained Foundation fork also available)
- __12-Factor__ based settings via `django-environ`
- Secure by default. We believe in SSL.
- Optimized development and production settings
- Registration via `django-allauth`
- Comes with custom user model ready to go
- Optional basic ASGI setup for Websockets
- Optional custom static build using Gulp and livereload
- Send emails via __Anymail__ (using Mailgun by default or __Amazon SES__ if AWS is selected cloud provider, but switchable)
- Media storage using Amazon S3 or Google Cloud Storage
- Docker support using `docker-compose` for development and production (using Traefik with LetsEncrypt support)
- `Procfile` for deploying to Heroku
- Instructions for deploying to PythonAnywhere
- Run tests with `unittest` or `pytest`
- Customizable __PostgreSQL__ version
- Default integration with `pre-commit` for identifying simple issues before submission to code review

#### Opcionales
- Serve static files from __Amazon S3__, __Google Cloud Storage__ or Whitenoise
- Configuration for __Celery__ and __Flower__ (the latter in Docker setup only)
- Integration with MailHog for local email testing
- Integration with Sentry for error logging


### Instalación
Instalamos cookiecutter
````
(.venv) pip install cookiecutter
````
Ajecutarlo contra el siguiente repositorio
````
(.venv) cookiecutter https://github.com/cookiecutter/cookiecutter-django
````

Responda las indicaciones con sus propias [opciones](https://cookiecutter-django.readthedocs.io/en/latest/project-generation-options.html).
````bash
project_name [My Awesome Project]: Django REST API
project_slug [django_rest_api]: 
description [Behold My Awesome Project!]: 
author_name [Daniel Roy Greenfeld]: Luis Camilo Jimenez
domain_name [example.com]: inertiasas.com
email [luis-camilo-jimenez@example.com]: luisca1985@gmail.com
version [0.1.0]: 
Select open_source_license:
1 - MIT
2 - BSD
3 - GPLv3
4 - Apache Software License 2.0
5 - Not open source
Choose from 1, 2, 3, 4, 5 [1]: 
timezone [UTC]: America/Bogota                    
windows [n]: 
use_pycharm [n]: 
use_docker [n]: y
Select postgresql_version:
1 - 13.2
2 - 12.6
3 - 11.11
4 - 10.16
Choose from 1, 2, 3, 4 [1]: 1
Select js_task_runner:
1 - None
2 - Gulp
Choose from 1, 2 [1]: 1
Select cloud_provider:
1 - AWS
2 - GCP
3 - None
Choose from 1, 2, 3 [1]: 1
Select mail_service:
1 - Mailgun
2 - Amazon SES
3 - Mailjet
4 - Mandrill
5 - Postmark
6 - Sendgrid
7 - SendinBlue
8 - SparkPost
9 - Other SMTP
Choose from 1, 2, 3, 4, 5, 6, 7, 8, 9 [1]: 2
use_async [n]: y
use_drf [n]: y
custom_bootstrap_compilation [n]: n
use_compressor [n]: n
use_celery [n]: y
use_mailhog [n]: n
use_sentry [n]: n
use_whitenoise [n]: y
use_heroku [n]: n
Select ci_tool:
1 - None
2 - Travis
3 - Gitlab
4 - Github
Choose from 1, 2, 3, 4 [1]: 1
keep_local_envs_in_vcs [y]: y
debug [n]: y
 [SUCCESS]: Project initialized, keep up the good work!
````

### Fuentes
- [cookiecutter-django](https://github.com/cookiecutter/cookiecutter-django#usage)
- [Cookiecutter Django's Documentation](https://cookiecutter-django.readthedocs.io/en/latest/)
- [Cookiecutter: Better Project Templates](https://cookiecutter.readthedocs.io/en/1.7.3/)
- [Creating your first app with Cookiecutter-Django](https://cookiecutter-django-webspired.readthedocs.io/en/latest/my-favorite-cookie.html)
- [cookiecutter-django Issues](https://github.com/cookiecutter/cookiecutter-django/issues/)


## Git
````
cd django_rest_api
git init --initial-branch=main
git remote add origin git@gitlab.com:grupo-principal/inertia-sas/django-rest-api.git
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
````
Se debe verificar que la versión de `git`, que la [opción `--initial-branch` se agregó en `git 2.28`](https://stackoverflow.com/questions/64787301/git-init-b-branch-name-command-in-terminal-is-throwing-an-unknown-switch).

Si aparece el error - [“fatal: refusing to merge unrelated histories” Git error](https://www.educative.io/edpresso/the-fatal-refusing-to-merge-unrelated-histories-git-error) se debe utilizar
````
git pull origin main --allow-unrelated-histories
````
### Fuentes
- [Start using Git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
- [Gitignore](https://git-scm.com/docs/gitignore)

### Instalacion de paquetes
Instalamos los paquetes de python.

````
pip3 install -r requirements/local.txt
````
Si se generea un error en algunos de los paquetes de la instalación [se puede utilizar de manera alternativa](https://stackoverflow.com/questions/22250483/stop-pip-from-failing-on-single-package-when-installing-with-requirements-txt) el cual ignora los errores.

````
cat requirements/local.txt | xargs -n 1 pip3 install
````

Verificamos todos los paquetes instalados.

````
pip3 freeze
````

Las versiones de los paquetes instalados pueden cambiar, dependiendo de la version de Django instalada.

### Fuentes
- [Pip User Guide - Requirements Files](https://pip.pypa.io/en/latest/user_guide/#requirements-files)


---

## Arquitectura de un Servicio Web
El Backend consiste en:

- Servidor
- Aplicación
- Base de Datos

Un Backend developer es un diseñador, su trabajo consiste un 90% en leer, diseñar, analizar y planear. Un 10% en programar. Nuestro trabajo más importante es el diseño del sistema y las decisiones tomadas son más costosas y más difíciles de cambiar.

Web Services es la manera en que se implementan las arquitecturas orientadas a servicios, se crean bloques que son accesibles a través de la web, son independientes del lenguaje de programación.

- __*SOAP*__: Tiene su propio estándar, conocido por utilizar XML.
- __*REST*__: Representational State Transfer, el objetivo es que nuestras operaciones sean Stateless. REST depende mucho más del protocolo HTTP.
- __*GraphQL*__: Es el más moderno, desarrollado por Facebook. Funciona más como un Query Language para las API, un lenguaje de consultas.

### Fuentes
- [Arquitectura de una Aplicacion - Presentacion](https://docs.google.com/presentation/d/1HlT7niPOISnHjk6ldbmCvZdiOeKqlzLNxfsGY5499_E/edit#slide=id.g4d4bf94f45_0_93)
- [Developer Roadmap](https://github.com/kamranahmedse/developer-roadmap)
- [API SOAP - usps](https://www.usps.com/business/web-tools-apis/welcome.htm)
- [API RESTful HTTP - Facebook](https://developers.facebook.com/docs/graph-api/)
- [API GaphQL - GitHub](https://docs.github.com/en/graphql)



## The Twelve-Factor App
Algunos principios de Twelve Factor app

- Formas declarativas de configuración
- Un contrato claro con el OS
- Listas para lanzar
- Minimizar la diferencia entre entornos
- Fácil de escalar

__*Codebase*__: Se refiere a que nuestra app siempre debe estar trackeada por un sistema de control de versiones como Git, Mercurial, etc. Una sola fuente de verdad.

__*Dependencias*__: Una 12 factor app nunca debe depender de la existencia implícita de nuestro OS, siempre se declaran explícitamente qué dependencias usa el proyecto y se encarga de que estas no se filtren. Dependency Isolation.

__*Configuración*__: Acá nos referimos a algo que va a cambiar durante entornos.

__*Backing services*__: Estos pueden ser conectados y desconectados a voluntad. Es cualquier servicio que nuestra aplicación puede consumir a través de la red como Base de Datos, Mensajería y Cola, Envío de Emails o Caché.

__*Build, release, run*__: Separa estrictamente las etapas de construcción y las de ejecución. Build es convertir nuestro código fuente en un paquete.Release es la etapa donde agregamos a nuestro paquete cosas de configuración como variables de entorno y Run donde corremos la aplicación en el entorno correspondiente. Las etapas no se pueden mezclar.

__*Procesos*__: En el caso más complejo tenemos muchos procesos corriendo como Celery y Redis, en esta parte los procesos son stateless y no comparten nada. Cualquier dato que necesite persistir en memoria o en disco duro tiene que almacenarse en un backing services,

__*Dev/prod parity*__: Reducir la diferencia entre entornos para reducir tiempo entre deploys y las personas involucradas sean las mismas que puedan hacer el deploy

__*Admin processes*__: Tratar los procesos administrativos como una cosa diferente, no deben estar con la app.

### Fuentes
- [The Twelve-Factor App](https://12factor.net/es/)

---

## Análisis del Código Base

__Objetivos__:
- Configuración _declarativa_.
- _Contrato claro_ con el OS.
- Lista para _lanzar_.
- _Reducir diferencia_ entre entornos.
- _Fácil de escalar_.

Se pueden alcanzar estos objetivos utilizando __Docker__, dado que:
- No necesita un "Guest OS".
- Usa muy poca memoria.
- Fácil de replicar y controlar.
- Fácil de compartir. 

__Servicio de Docker del proyecto__:
- Django (port: 8000)
- PostgreSQL (port: 5432)
- Redis (port: 6379)
- Celery (port: 5555). Tiene tres servicios.
  - Brocker
  - Bit
  - Flower: Es una interfaz visual que permite ver que está sucediendo en tiempo real con Celery. Es el que realmente está corriendo en el puerto 5555.

### Config y Settings

````
config
|-- __init__.py
|-- api_router.py
|-- asgi.py
|-- celery_app.py
|-- settings
|   |-- __init__.py
|   |-- base.py
|   |-- local.py
|   |-- production.py
|   `-- test.py
|-- urls.py
|-- websocket.py
`-- wsgi.py
````

### Requirements
````
requirements
|-- base.txt
|-- local.txt
`-- production.txt
````

### Docker
````
.
|-- compose
|   |-- local
|   |   |-- django
|   |   |   |-- Dockerfile
|   |   |   |-- celery
|   |   |   |   |-- beat
|   |   |   |   |   `-- start
|   |   |   |   |-- flower
|   |   |   |   |   `-- start
|   |   |   |   `-- worker
|   |   |   |       `-- start
|   |   |   `-- start
|   |   `-- docs
|   |       |-- Dockerfile
|   |       `-- start
|   `-- production
|       |-- aws
|       |   |-- Dockerfile
|       |   `-- maintenance
|       |       |-- download
|       |       `-- upload
|       |-- django
|       |   |-- Dockerfile
|       |   |-- celery
|       |   |   |-- beat
|       |   |   |   `-- start
|       |   |   |-- flower
|       |   |   |   `-- start
|       |   |   `-- worker
|       |   |       `-- start
|       |   |-- entrypoint
|       |   `-- start
|       |-- postgres
|       |   |-- Dockerfile
|       |   `-- maintenance
|       |       |-- _sourced
|       |       |   |-- constants.sh
|       |       |   |-- countdown.sh
|       |       |   |-- messages.sh
|       |       |   `-- yes_no.sh
|       |       |-- backup
|       |       |-- backups
|       |       `-- restore
|       `-- traefik
|           |-- Dockerfile
|           `-- traefik.yml
|-- local.yml
|-- ...
|-- production.yml
|-- ...
:
````

### django_rest_api
````
django_rest_api
|-- __init__.py
|-- conftest.py
|-- contrib
|   |-- __init__.py
|   `-- sites
|       |-- __init__.py
|       `-- migrations
|           |-- 0001_initial.py
|           |-- 0002_alter_domain_unique.py
|           |-- 0003_set_site_domain_and_name.py
|           |-- 0004_alter_options_ordering_domain.py
|           `-- __init__.py
|-- static
|   |-- css
|   |   `-- project.css
|   |-- fonts
|   |-- images
|   |   `-- favicons
|   |       `-- favicon.ico
|   |-- js
|   |   `-- project.js
|   `-- sass
|       |-- custom_bootstrap_vars.scss
|       `-- project.scss
|-- templates
|   |-- 403.html
|   |-- 404.html
|   |-- 500.html
|   |-- account
|   |   |-- account_inactive.html
|   |   |-- base.html
|   |   |-- email.html
|   |   |-- email_confirm.html
|   |   |-- login.html
|   |   |-- logout.html
|   |   |-- password_change.html
|   |   |-- password_reset.html
|   |   |-- password_reset_done.html
|   |   |-- password_reset_from_key.html
|   |   |-- password_reset_from_key_done.html
|   |   |-- password_set.html
|   |   |-- signup.html
|   |   |-- signup_closed.html
|   |   |-- verification_sent.html
|   |   `-- verified_email_required.html
|   |-- base.html
|   |-- pages
|   |   |-- about.html
|   |   `-- home.html
|   `-- users
|       |-- user_detail.html
|       `-- user_form.html
|-- users
|   |-- __init__.py
|   |-- adapters.py
|   |-- admin.py
|   |-- api
|   |   |-- serializers.py
|   |   `-- views.py
|   |-- apps.py
|   |-- forms.py
|   |-- migrations
|   |   |-- 0001_initial.py
|   |   `-- __init__.py
|   |-- models.py
|   |-- tasks.py
|   |-- tests
|   |   |-- __init__.py
|   |   |-- factories.py
|   |   |-- test_admin.py
|   |   |-- test_drf_urls.py
|   |   |-- test_drf_views.py
|   |   |-- test_forms.py
|   |   |-- test_models.py
|   |   |-- test_tasks.py
|   |   |-- test_urls.py
|   |   `-- test_views.py
|   |-- urls.py
|   `-- views.py
`-- utils
    |-- __init__.py
    |-- context_processors.py
    `-- storages.py
````
---

## Docker
### Build the Stack

This can take a while, especially the first time you run this particular command on your development system:

````
docker-compose -f local.yml build
````

### Run the Stack

````
docker-compose -f local.yml up
````

You can also set the environment variable COMPOSE_FILE pointing to local.yml like this:
````
export COMPOSE_FILE=local.yml
````

And then run:
````
docker-compose up
````
To run in a detached (background) mode, just:
````
docker-compose up -d
````
### Execute Management Commands
````gi
docker-compose -f local.yml run --rm django python manage.py migrate
````
````
docker-compose -f local.yml run --rm django python manage.py createsuperuser
````

### Debugging

#### ipdb

Si se está utilizando `ipdb` dentro del código para depurar:
````
import ipdb; ipdb.set_trace()
````

Entonces es posible que deba correr el contenedor `django` para que funcione.
````
$ docker-compose -f local.yml run --rm --service-ports django
````
Si el proceso `django` ya está corriendo es necesario matarlo primero. Buscamos el nombre del proceso.
````
docker-compose ps
````
Después se remueve
````
docker rm -f <django-ps-name>
````
(Nota: Recordar incluir antes `export COMPOSE_FILE=local.yml`)
### Fuentes
- [Cookiecutter Django With Docker](https://cookiecutter-django.readthedocs.io/en/latest/developing-locally-docker.html)
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Docker Compose CLI reference](https://docs.docker.com/compose/reference/)
- [Docker CLI reference](https://docs.docker.com/engine/reference/commandline/docker/)

---

## Modelos Abstractos
La herencia de modelos puede ser útil porque podemos tener datos generales que pueden ser heredados por otras que no necesariamente tienen su propia tabla, porque queremos que haya herencia de múltiples tablas que se reflejan en la base de datos o porque queremos extender la funcionalidad de un modelo.

### Abstract base classes Example

````py
from django.db import models

class CommonInfo(models.Model):
    name = models.CharField(max_length=100)
    age = models.PositiveIntegerField()

    class Meta:
        abstract = True

class Student(CommonInfo):
    home_group = models.CharField(max_length=5)
````
### cride/utils/models.py

````py
"""Django models utilities."""

# Django
from django.db import models


class CRideModel(models.Model):
    """Comparte Ride base model.
    CRideModel acts as an abstract base class from which every
    other model in the project will inherit. This class provides
    every table with the following attributes:
        + created (DateTime): Store the datetime the object was created.
        + modified (DateTime): Store the last datetime the object was modified.
    """

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )
    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time on which the object was last modified.'
    )

    class Meta:
        """Meta option."""

        asbtract = True

        get_latest_by = 'created'
        ordering = ['-created', '-modified']
````

### Fuentes
- [Django Model inheritance](https://docs.djangoproject.com/en/3.2/topics/db/models/#model-inheritance)
- [Django Model Meta options](https://docs.djangoproject.com/en/3.2/ref/models/options/)

## Modelos Proxy
Los __Proxys__ nos permiten extender la funcionalidad de un modelo sin crear una nueva tabla en la base de datos, la diferencia con los Abstract Models es que estas solo exponen un molde de atributos y las proxys extienden de una tabla ya existente.

### Proxy models Example
````py
from django.db import models

class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

class MyPerson(Person):
    class Meta:
        proxy = True

    def do_something(self):
        # ...
        pass
````
````py
>>> p = Person.objects.create(first_name="foobar")
>>> MyPerson.objects.get(first_name="foobar")
<MyPerson: foobar>
````
### cride/utils/models.py

````py
"""Django models utilities."""

# Django
from django.db import models


class CRideModel(models.Model):
    """Comparte Ride base model.
    CRideModel acts as an abstract base class from which every
    other model in the project will inherit. This class provides
    every table with the following attributes:
        + created (DateTime): Store the datetime the object was created.
        + modified (DateTime): Store the last datetime the object was modified.
    """

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )
    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time on which the object was last modified.'
    )

    class Meta:
        """Meta option."""

        asbtract = True

        get_latest_by = 'created'
        ordering = ['-created', '-modified']



class Person(models.Model):
    first_name = models.CharField()
    last_name = models.CharField()


class MyPerson(Person):
    class Meta:
        proxy = True

    def say_hi(name):
        pass


MyPerson.objects.all()
ricardo = MyPerson.objects.get(pk=1)
ricardo.say_hi('Pablo')

rulo = Person.objects.get(pk=2)
rulo.say_hi('Pablo')
````

### Fuentes
- [Django Proxy Models](https://docs.djangoproject.com/en/3.2/topics/db/models/#proxy-models)

## App de Usuarios

### Típica ubicación para crear una aplicación
1. Crear la aplicación `<name-of-the-app>` con python manage.py startapp
2. Mover el directorio `<name-of-the-app>` al directorio `<project_slug>`
3. Editar `<project_slug>/<name-of-the-app>/apps.py` cambiar `name = "<name-of-the-app>"` a `name = "<project_slug>.<name-of-the-app>"`
4. Agregar `"<project_slug>.<name-of-the-app>.apps.<NameOfTheAppConfigClass>"`, en [LOCAL_APPS on `config/settings/base.py`](https://github.com/cookiecutter/cookiecutter-django/blob/175381213672b409f940730c2bafc129815d5595/%7B%7Bcookiecutter.project_slug%7D%7D/config/settings/base.py#L79)


Si no se utiliza startapp, simplementes debe crear un módulo de Django con `apps.py`, y referenciar la aplicación en `settings`

#### app.py
````py
# apps.py in your new shiny Django app under the project's root `<project_slug>/` dir

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class <YourCamelCaseAppName>AppConfig(AppConfig):
    name = "<project_slug>.<your_snake_case_app_name>"
    verbose_name = _("<Your app brand name>")

    def ready(self):
        try:
            # noinspection PyUnresolvedReferences
            from . import signals  # noqa F401
        except ImportError:
            pass
````
And this where you should install your new app:

### config/settings/base.py
````py
# `base.py`

# ...
LOCAL_APPS = [
    # ...
    "<project_slug>.<your_snake_case_app_name>.apps.<YourCamelCaseAppName>AppConfig",
    # ...
]
# ...
````

Si se necesita la aplicación instalada únicamente en un entorno específico y se cuente con un archivo de configuración asociado como `local.py` o `production.py`:

````py
# anywhere else but in `base.py`

# ...
INSTALLED_APPS += ["<project_slug>.<your_snake_case_app_name>.apps.<YourCamelCaseAppName>AppConfig"]
# ...
````


### cride/users/apps.py
````py
"""Users app."""

# Django
from django.apps import AppConfig


class UsersAppConfig(AppConfig):
    """Users app config."""

    name = 'cride.users'
    verbose_name = 'Users'
````
### config/settings/base.py
````py
LOCAL_APPS = [
    'cride.users.apps.UsersAppConfig',
]
````

### Fuentes
- [New apps created are located in the project root, not inside the project slug](https://github.com/cookiecutter/cookiecutter-django/issues/1725)
- [Typical place to create app in cookiecutter-django](https://github.com/cookiecutter/cookiecutter-django/issues/1876)
- [Django Extending the existing User model](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#extending-the-existing-user-model)
- [Django Using a custom user model when starting a project](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project)
- [django.contrib.auth.models - AbstractUser](https://github.com/django/django/blob/main/django/contrib/auth/models.py#L324)
- [Django Specifying a custom user model](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#specifying-a-custom-user-model)
- [Django Specifying a custom user model - USERNAME_FIELD](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#django.contrib.auth.models.CustomUser.USERNAME_FIELD)
- [Django Specifying a custom user model - REQUIRED_FIELDS](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#django.contrib.auth.models.CustomUser.REQUIRED_FIELDS)
- [Django INSTALLED_APPS](https://docs.djangoproject.com/en/3.2/ref/settings/#installed-apps)
- [Django Github](https://github.com/django/django/tree/stable/3.2.x)
- [Django Models](https://github.com/django/django)

## Organizando modelos en un paquete de Django
Deconstruir el modelo de Usuario en múltiples archivos dentro de un paquete

__Sustitución del modelo `User` personalizado__
1. Creamos la clase `User` heredando de `AbstractUser`
2. Agregamos todos los campos que nuestra aplicación necesite.
3. En el archivo de configuración (`base.py`), agregamos: `AUTH_USER_MODEL = 'users.User'`

__Organizar modelos en múltiples archivos__
1. Creamos el directorio `models`.
2. Ccreamos el archivo `__init__.py`, 
2. Creamos los archivos correspondientes a los modelos (`users.py`, `profiles.py`, etc)
3. Dentro del archivo `__init__.py` importamos los modelos: from .users import User

### Fuentes
- [django.contrib.auth.models - AbstractUser - get_short_name](https://github.com/django/django/blob/main/django/contrib/auth/models.py#L383)
- [Django Writing validators](https://docs.djangoproject.com/en/3.2/ref/validators/#writing-validators)
- [Django RegexValidator](https://docs.djangoproject.com/en/3.2/ref/validators/#regexvalidator)
- [Django django.core.validators](https://docs.djangoproject.com/es/2.2/_modules/django/core/validators/)
- [django.core.validators - RegexValidator](https://github.com/django/django/blob/main/django/core/validators.py#L18)
- [Django Reusable apps and AUTH_USER_MODEL](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#reusable-apps-and-auth-user-model)
- [Django Referencing the User model](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#referencing-the-user-model)

## Creando el modelo de perfil de usuario

__Campos de AbstractUser__
1. `username` (CharField)
2. `first_name` (CharField)
3. `last_name` (CharField)
4. `email` (EmailField)
5. `is_staff` (BooleanField)
6. `is_active` (BooleanField)
7. `date_joined` (DateTimeField)

__Campos de AbstractBaseUser__
1. `password` (CharField)
2. `last_login` (DateTimeField)
3. `is_active` = True

### Fuentes
- [Django Model field reference](https://docs.djangoproject.com/en/3.2/ref/models/fields/)
- [Django Model field reference - OneToOneField](https://docs.djangoproject.com/en/3.2/ref/models/fields/#django.db.models.OneToOneField)
- [Django Model field reference - Relationship fields - Arguments - ForeignKey.on_delete](https://docs.djangoproject.com/en/3.2/ref/models/fields/#django.db.models.ForeignKey.on_delete)
- [ModelAdmin objects](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#modeladmin-objects)
- [django.contrib.auth.admin.py - UserAdmin](https://github.com/django/django/blob/main/django/contrib/auth/admin.py#L41)
- [Django Migrations](https://docs.djangoproject.com/en/3.2/topics/migrations/)
- [Django Migrations - Squashing migrations](https://docs.djangoproject.com/en/3.2/topics/migrations/#migration-squashing)

---

## Aplicación y modelo de círculos

````
circles
|-- migrations
|-- models
|   |-- __init__.py
|   `-- circles.py
|-- __init.py__
`-- apps.py
````
### Fuentes
- [New apps created are located in the project root, not inside the project slug](https://github.com/cookiecutter/cookiecutter-django/issues/1725)
- [Typical place to create app in cookiecutter-django](https://github.com/cookiecutter/cookiecutter-django/issues/1876)
- [Django Model inheritance](https://docs.djangoproject.com/en/3.2/topics/db/models/#model-inheritance)
- [Django Model Meta options](https://docs.djangoproject.com/en/3.2/ref/models/options/)
- [Django Model field reference - CharField](https://docs.djangoproject.com/en/3.2/ref/models/fields/#charfield)
- [Django Model field reference - SlugField](https://docs.djangoproject.com/en/3.2/ref/models/fields/#slugfield)
- [Django Model field reference - ImageField](https://docs.djangoproject.com/en/3.2/ref/models/fields/#imagefield)
- [Django Model field reference - PositiveIntegerField](https://docs.djangoproject.com/en/3.2/ref/models/fields/#positiveintegerfield)
- [Django Model field reference - BooleanField](https://docs.djangoproject.com/en/3.2/ref/models/fields/#booleanfield)

## Migraciones y admin de círculos
````
circles
|-- migrations
|   |-- __init__.py
|   `-- 00001_ini...
|-- models
|   |-- __init__.py
|   `-- circles.py
|-- __init.py__
|-- admin.py
`-- apps.py
````
### Fuentes
- [Django Migrations](https://docs.djangoproject.com/en/3.2/topics/migrations/)
- [Django The Django admin site - ModelAdmin objects](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#modeladmin-objects)
- [Django The Django admin site - The register decorator](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#the-register-decorator)
- [Django The Django admin site - ModelAdmin.list_display](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#django.contrib.admin.ModelAdmin.list_display)
- [Django The Django admin site - ModelAdmin.search_fields](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#django.contrib.admin.ModelAdmin.search_fields)
- [Django The Django admin site - ModelAdmin.list_filter](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#django.contrib.admin.ModelAdmin.list_filter)

## Shell Plus
````
docker-compose run --rm django python manage.py shell_plus
````
### Fuentes
- [django-extensions](https://django-extensions.readthedocs.io/en/latest/)
- [django-extensions - Installation instructions](https://django-extensions.readthedocs.io/en/latest/installation_instructions.html)
- [django-extensions - shell_plus](https://django-extensions.readthedocs.io/en/latest/shell_plus.html?highlight=shell_plus#)

## Construir tu propio API con Django Rest Framework
Django Rest Framework es una librería que cuenta con muchas herramientas para poder crear nuestras APIs con ayuda de Django. Tiene algunos beneficios como políticas de autenticación incluyendo packetes de OAuth1 y OAuth2, serialización de datos que soporta ORM (Object-relational Mapping), puedes hacer uso de las populares Class Based Views y Function Based Views si necesitas algo más personalizado. Empresas como Mozilla, Red Hat, Heroku y Eventbrit lo utilizan.

````
circles
|-- migrations
|   |-- __init__.py
|   `-- 00001_ini...
|-- models
|   |-- __init__.py
|   `-- circles.py
|-- __init.py__
|-- admin.py
|-- apps.py
|-- urls.py
`-- views.py
````

### Fuentes
- [Django URL dispatcher - Including other URLconfs](https://docs.djangoproject.com/en/3.2/topics/http/urls/#including-other-urlconfs)
- [Django URL dispatcher - URL namespaces](https://docs.djangoproject.com/en/3.2/topics/http/urls/#url-namespaces)
- [Django Writing views](https://docs.djangoproject.com/en/3.2/topics/http/views/)
- [HTTPie](https://httpie.io/)
- [Django Request and response objects - JsonResponse objects](https://docs.djangoproject.com/en/3.2/ref/request-response/#jsonresponse-objects)

## Vistas, URLs y Parsers de Django REST framework
````
circles
|-- migrations
|   |-- __init__.py
|   `-- 00001_ini...
|-- models
|   |-- __init__.py
|   `-- circles.py
|-- __init.py__
|-- admin.py
|-- apps.py
|-- urls.py
`-- views.py
````
### Fuentes
- [Django REST framework](https://www.django-rest-framework.org/)
- [Github - encode/django-rest-framework](https://github.com/encode/django-rest-framework)
- [Django REST framework - Class-based Views](https://www.django-rest-framework.org/api-guide/views/)
- [Django REST framework - Class-based Views - @api_view()](https://www.django-rest-framework.org/api-guide/views/#api_view)
- [Django REST framework - Requests](https://www.django-rest-framework.org/api-guide/requests/)

## Serializers
Los serializers son contenedores que nos permiten tomar tipos de datos complejos, convertirlos en datos nativos de python para después poderlos usar como JSON o XML. Son contenedores que amoldan datos para que cumplan con las condiciones de los serializers y sean llevados a un tipo de estos y después estos puedan ser transformados en otra cosa.

````
circles
|-- migrations
|   |-- __init__.py
|   `-- 00001_ini...
|-- models
|   |-- __init__.py
|   `-- circles.py
|-- __init.py__
|-- admin.py
|-- apps.py
|-- serializers.py
|-- urls.py
`-- views.py
````

### Fuentes
- [Django REST framework - Serializers](https://www.django-rest-framework.org/api-guide/serializers/)
- [Django REST framework - Serializer fields](https://www.django-rest-framework.org/api-guide/fields/)
- [Django REST framework - Serializer fields - CharField](https://www.django-rest-framework.org/api-guide/fields/#charfield)
- [Django REST framework - Serializer fields - SlugField](https://www.django-rest-framework.org/api-guide/fields/#slugfield)
- [Django REST framework - Serializer fields - IntegerField](https://www.django-rest-framework.org/api-guide/fields/#integerfield)
- [Django REST framework - Serializers - Serializing objects](https://www.django-rest-framework.org/api-guide/serializers/#serializing-objects)
- [Django REST framework - Serializers - Dealing with multiple objects](https://www.django-rest-framework.org/api-guide/serializers/#dealing-with-multiple-objects)
- [Django REST framework - Serializers - Validation](https://www.django-rest-framework.org/api-guide/serializers/#validation)
- [Django REST framework - Serializers - Saving instances](https://www.django-rest-framework.org/api-guide/serializers/#saving-instances)
- [Django REST framework - Validators - UniqueValidator](https://www.django-rest-framework.org/api-guide/validators/#uniquevalidator)
- [Django REST Framework Serializando modelos que tienen campos/relaciones ForeignKey y ManyToManyFields](https://es.stackoverflow.com/questions/5419/django-rest-framework-serializando-modelos-que-tienen-campos-relaciones-foreignk#)

## Buenas prácticas para el diseño de un API REST
Uno de los prerequisitos para crear APIs es conocer el protocolo HTTP. Verbos, métodos, estados y las cabeceras.

Van a estar diseñando una interfaz para programadores para que otros programadores puedan interactuar, nos olvidaremos de los templates para que un equipo de Frontend se encargue de eso. Debemos tener la perspectiva de un usuario de API y no la de un diseñador de API.

El _objetivo_ es algo que siempre se deben preguntar qué problema deben de resolverle al usuario final nuestra API. El _éxito_ de nuestra API se mide por qué tan rápido nuestros compañeros pueden usarla.

__REST__: Es una serie de principio de cómo diseñar una web service. Un estilo de arquitectura.

__HTTP Status Code__:

- 201: Creado
- 304: No modificado
- 404: No encontrado
- 401: No autorizado
- 403: Prohibido o restringido.

__Pro tips__:

- SSL
- Caché
- Valida
- CSRF o Cross-Site Request Forgery
- Limita los requests
- Complementa tu API con un SDK

### Fuentes
- [Web API Design - Crafting Interfaces that Developers Love - apigee](https://pages.apigee.com/rs/apigee/images/api-design-ebook-2012-03.pdf)
- [API Graph - Facebook for Developers](https://developers.facebook.com/docs/graph-api/)
- [APIs Design - Speaker Deck](https://speakerdeck.com/pablotrinidad/apis-design?slide=12)
- [List of HTTP status codes - Wikipedia](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
- [Swagger](https://swagger.io/)
- [Django REST Swagger](https://django-rest-swagger.readthedocs.io/en/latest/)

## Request, response, renderers y parsers
### Fuentes
- [Django REST framework - Requests](https://www.django-rest-framework.org/api-guide/requests/)
- [Django REST framework - Responses](https://www.django-rest-framework.org/api-guide/responses/)
- [Django REST framework - Parsers](https://www.django-rest-framework.org/api-guide/parsers/)
- [Django REST framework - Renderers](https://www.django-rest-framework.org/api-guide/renderers/)
- [Django REST framework - Renderers - Setting the renderers](https://www.django-rest-framework.org/api-guide/renderers/#setting-the-renderers)
- [HTTP Status Codes](https://httpstatuses.com/)

## Autenticación y tipos de autenticación
La autenticación es la parte de asociar una petición a un usuario y después al objeto request se le asigna dos propiedades como request.user y request.auth

### Fuentes
- [Django REST framework - Authentication](https://www.django-rest-framework.org/api-guide/authentication/)
- [Django REST framework - Permissions](https://www.django-rest-framework.org/api-guide/permissions/)
- [Basic access authentication - Wikipedia](https://en.wikipedia.org/wiki/Basic_access_authentication)
- [Django REST framework - Authentication - TokenAuthentication](https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication)
- [OAuth - Wikipwdia](https://en.wikipedia.org/wiki/OAuth)
- [Django REST framework - Authentication - Django OAuth Toolkit](https://www.django-rest-framework.org/api-guide/authentication/#django-oauth-toolkit)
- [Django REST framework - Authentication - Django REST framework OAuth](https://www.django-rest-framework.org/api-guide/authentication/#django-rest-framework-oauth)
- [JSON Web Tokens JWT.IO](https://jwt.io/)
- [Django REST framework - Authentication - JSON Web Token Authentication](https://www.django-rest-framework.org/api-guide/authentication/#json-web-token-authentication)

## APIView
### Fuentes
- [Django REST framework - Class-based Views](https://www.django-rest-framework.org/api-guide/views/)
- [Classy Django REST framework - APIView](https://www.cdrf.co/3.12/rest_framework.views/APIView.html)
- [Classy Django REST framework - APIView - dispatch](https://www.cdrf.co/3.12/rest_framework.views/APIView.html#dispatch)
- [REST framework - Status Codes](https://www.django-rest-framework.org/api-guide/status-codes/)
- [Django REST framework - Serializer fields - CharField](https://www.django-rest-framework.org/api-guide/fields/#charfield)
- [Django REST framework - Serializer fields - EmailField](https://www.django-rest-framework.org/api-guide/fields/#emailfield)
- [Django REST framework - Serializer - Validation](https://www.django-rest-framework.org/api-guide/serializers/#validation)
- [Django REST framework - Serializer - Validation - Object-level validation](https://www.django-rest-framework.org/api-guide/serializers/#object-level-validation)
- [Django - Authenticating users](https://docs.djangoproject.com/en/3.2/topics/auth/default/#authenticating-users)

## Creando el token de autorización (login)
### Fuentes
- [Django REST framework - Authentication - TokenAuthentication](https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication)
- [django-rest-framework/rest_framework/authtoken/models.py - class Token](https://github.com/encode/django-rest-framework/blob/master/rest_framework/authtoken/models.py#L9)
- [Django REST framework - Serializers - ModelSerializer](https://www.django-rest-framework.org/api-guide/serializers/#modelserializer)
- [Simple JWT](https://django-rest-framework-simplejwt.readthedocs.io/en/latest/)

## User sign up
### Fuentes
- [Django REST framework - Serializers](https://www.django-rest-framework.org/api-guide/serializers/)
- [Django REST framework - Serializer fields](https://www.django-rest-framework.org/api-guide/fields/)
- [Django REST framework - Validators](https://www.django-rest-framework.org/api-guide/validators/)
- [Django - Validators - RegexValidator](https://docs.djangoproject.com/en/3.2/ref/validators/#regexvalidator)
- [Django REST framework - Serializers - Validation - Object-level validation](https://www.django-rest-framework.org/api-guide/serializers/#object-level-validation)
- [Django - Password management in Django - Password validation - validate_password](https://docs.djangoproject.com/en/3.2/topics/auth/passwords/#django.contrib.auth.password_validation.validate_password)
- [Django REST framework - Serializers - Saving instances](https://www.django-rest-framework.org/api-guide/serializers/#saving-instances)
- [Django - Using the Django authentication system - Creating users](https://docs.djangoproject.com/en/3.2/topics/auth/default/#creating-users)
- [Django REST framework - Authentication - TokenAuthentication](https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication)

## Limitar login a usuarios con cuenta verificada
### Fuentes
- [Django REST framework - Exeptions - ValidationError](https://www.django-rest-framework.org/api-guide/exceptions/#validationerror)

## Configurar envío de email
### Fuentes
- [Github - django-anymail](https://github.com/anymail/django-anymail)
- [Django - Sending email](https://docs.djangoproject.com/en/3.2/topics/email/)
- [Django - Templates - render_to_string](https://docs.djangoproject.com/en/3.2/topics/templates/#django.template.loader.render_to_string)
- [Github - PyJWT](https://github.com/jpadilla/pyjwt)
- [PyJWT](https://pyjwt.readthedocs.io/en/latest/)

## Instalar PyJWT y generar tokens
### Fuentes
- [Github PyJWT](https://github.com/jpadilla/pyjwt)
- [PyJWT - Registered Claim Names](https://pyjwt.readthedocs.io/en/latest/usage.html#registered-claim-names)
- [JWT.io](https://jwt.io/)
- [Django - Utils - django.utils.timezone](https://docs.djangoproject.com/en/3.2/ref/utils/#module-django.utils.timezone)
- [Python - datetime - timedelta](https://docs.python.org/es/3/library/datetime.html#timedelta-objects)
- [PyJWT - Digital Signature Algorithms](https://pyjwt.readthedocs.io/en/latest/algorithms.html)

## Verificar cuenta usando JWT
### Fuentes
- [Django REST framework - Serializers - Saving instances](https://www.django-rest-framework.org/api-guide/serializers/#saving-instances)
- [Github PyJWT - Exceptions](https://github.com/jpadilla/pyjwt/blob/master/jwt/exceptions.py)
- [PyJWT - Exceptions - ExpiredSignatureError](https://pyjwt.readthedocs.io/en/latest/api.html#jwt.exceptions.ExpiredSignatureError)
- [Django REST framework - Exceptions - ValidationError](https://www.django-rest-framework.org/api-guide/exceptions/#validationerror)
- [Django REST framework - Serializers - Saving instances - Overriding .save() directly](https://www.django-rest-framework.org/api-guide/serializers/#overriding-save-directly)
- [How to Use JWT Authentication with Django REST Framework](https://simpleisbetterthancomplex.com/tutorial/2018/12/19/how-to-use-jwt-authentication-with-django-rest-framework.html)

## Actualizar modelo de circle (membership)
### Fuentes
- [Django - Models - Relationships - Extra fields on many-to-many relationships](https://docs.djangoproject.com/en/3.2/topics/db/models/#extra-fields-on-many-to-many-relationships)
- [Django - Model field reference - Relationship fields - ForeignKey](https://docs.djangoproject.com/en/3.2/ref/models/fields/#foreignkey)

## Crear CircleViewSet
### Fuentes
- [Django REST framework - Generic views](https://www.django-rest-framework.org/api-guide/generic-views/)
- [Django REST framework - ViewSets](https://www.django-rest-framework.org/api-guide/viewsets/)
- [Github django-rest-framework](https://github.com/encode/django-rest-framework)